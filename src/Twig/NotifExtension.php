<?php
namespace Cpamg\NotifBundle\Twig;

class NotifExtension extends \Twig_Extension
{
    public function getFilters()
    {

        return [
          new \Twig_SimpleFilter('notif', [
            $this,
            'notif'
          ], ['is_safe' => ['html']]),
        ];
    }

    public function notif(
      $data,
      $type = "success",
      $expires = false,
      $horizontal = "right",
      $vertical = "top",
      $header = false
    ) {

        $aff = "";
        if (is_array($data)) {
            foreach ($data as $valeur) {
                $aff .= $this->buildRowNotification($valeur,$type,$expires,$horizontal,$vertical,$header);
            }
        } else {
            $aff .= $this->buildRowNotification($data,$type,$expires,$horizontal,$vertical,$header);
        }

        return $this->buildNotification($aff);
    }

    protected function buildRowNotification(
      $valeur,
      $type = "success",
      $expires = false,
      $horizontal = "right",
      $vertical = "top",
      $header = false
    ) {

        if (is_object($valeur)) {
            $message = $valeur->getMessage();
            $typeNotif = !$valeur->getType() ? $type : $valeur->getType();
            $expiresNotif = $valeur->getExpires() == "unlimited" ? $expires : (!$valeur->getExpires() ? "false" : $valeur->getExpires());
            $horizontalNotif = !$valeur->getHorizontal() ? $horizontal : $valeur->getHorizontal();
            $verticalNotif = !$valeur->getVertical() ? $vertical : $valeur->getVertical();
            $headerNotif = !$valeur->getHeader() ? $header : $valeur->getHeader();
        } else {
            $message = $valeur;
            $typeNotif = $type;
            $expiresNotif = $expires;
            $horizontalNotif = $horizontal;
            $verticalNotif = $vertical;
            $headerNotif = $header;
        }
        if($message != ""){
          return $this->createNotification($message, $typeNotif, $expiresNotif, $horizontalNotif, $verticalNotif, $headerNotif);
        }
    }

    protected function checkData($array, $key, $default)
    {

        return is_array($array) && array_key_exists($key, $array) ? $array[$key] : $default;
    }

    protected function createNotification(
      $message,
      $type,
      $expires,
      $horizontal,
      $vertical,
      $header
    ) {

        $message = nl2br($message);

        $message = str_replace("\n", '', $message);
        $message = str_replace("\r", '', $message);
        $message = str_replace('"', '', $message);

       
        $expires = !$expires || $expires == "false" ? '"false"' : $expires;
        $header = !$header ? "false" : $header;

        return 'n.getFlash(n.setFlash("' . $message . '","' . $type . '","' . $header . '"),' . $expires . ',"' . $horizontal . '","' . $vertical . '");';
    }

    protected function buildNotification($messages)
    {

        $aff = <<<HTML
        <script type="text/javascript" language="javascript">
            $(document).ready(function(e) {
                var n = new SoNotification;
                {$messages}
            });
        </script>
HTML;

        return $aff;
    }

    public function getName()
    {

        return 'cpamg_notif';
    }
}