var SoNotification = function () {

    this.id_element = "soContainer";
    this.horizontal = "top";
    this.vertical = "right";
    this.expires = "false";
    this.typeFlash = "";
    this.messageFlash = "";
    this.messagePhpFlash = "";
    this.headerFlash = "";
    this.separateur = "[]";


    /**
     *    Getter separateur
     *    @param      :
     *    @return     :
     *    @exemple    :
     */
    this.getSeparateur = function () {
        return this.separateur;
    }

    /**
     *    Getter id container permettant dafficher la notif
     *    @param      :
     *    @return     :
     *    @exemple    :
     */
    this.getIdContainer = function () {
        return this.id_element;
    }


    /**
     *    Setter flash message
     *    @param      : Message a afficher
     *    @param      : Type de notification valide,erreur,attention,infos
     *    @return     : varchar pre formate
     *    @exemple    :
     */
    this.setFlash = function (message, type, header) {

        if (typeof(type) == "undefined")
            type = "success";

        if (typeof(header) == "undefined")
            header = "false";

        return this.getSeparateur() + type + this.getSeparateur() + message + this.getSeparateur() + header;
    }

    this.setHeaderFlash = function (message) {
        if (typeof(message) == "undefined")
            message = "false";

        this.headerFlash = message;
    }

    this.getHeaderFlash = function () {
        return this.headerFlash;
    }
    /**
     *    Getter du retour message php, via AJAX
     *    @param      :
     *    @return     : varchar
     *    @exemple    :
     */
    this.getMessagePhpFlash = function () {
        return this.messagePhpFlash;
    }

    /**
     *    Setter Type de notification
     *    @param      : varchar
     *    @return     : varchar
     *    @exemple    :
     */
    this.setTypeFlash = function (type_message) {
        this.typeFlash = type_message;
    }

    /**
     *    Setter Message Flash
     *    @param      : varchar
     *    @return     : varchar
     *    @exemple    :
     */
    this.setMessageFlash = function (message) {
        this.messageFlash = message;
    }

    /**
     *    Setter Debug Flash
     *    @param      : varchar
     *    @return     : varchar
     *    @exemple    :
     */
    this.setDebugFlash = function (message) {
        this.debugFlash = message;
    }

    /**
     *    Getter Type flash
     *    @param      :
     *    @return     : varchar
     *    @exemple    :
     */
    this.getTypeFlash = function () {
        return this.typeFlash;
    }

    /**
     *    Getter Message flash
     *    @param      :
     *    @return     : varchar
     *    @exemple    :
     */
    this.getMessageFlash = function () {
        return this.messageFlash;
    }

    /**
     *    Getter Message debug permet de catcher les warning, notice ou autres messages php
     *    @param      :
     *    @return     :
     *    @exemple    :
     */
    this.getDebugFlash = function () {
        return this.debugFlash;
    }

    this.getDebug = function () {
        if (this.getDebugFlash() != "")
            this.getFlash(this.setFlash(this.getDebugFlash(), "warning"), "false");
    }

    /**
     *    Getter retournant le message php
     *    @param      :
     *    @return     : varchar
     *    @exemple    :
     */
    this.getFlashPhp = function (data) {
        var message = this.getSeparateur() + 'error' + this.getSeparateur() + "Bad format" + this.getSeparateur() + "false";
        var expires = "false";
        var horizontal = "rigth";
        var vertical = "top";

        if (typeof (data.message) !== "undefined" && typeof (data.type) !== "undefined") {
            message = this.getSeparateur() + data.type + this.getSeparateur() + data.message + this.getSeparateur() + data.header;
        }

        if (typeof (data.expires) !== "undefined") {
            expires = data.expires;
        }

        if (typeof (data.horizontal) !== "undefined" && data.horizontal !== false) {
            horizontal = data.horizontal;
        }

        if (typeof (data.vertical) !== "undefined" && data.vertical !== false) {
            vertical = data.vertical;
        }

        this.getFlash(message, expires, horizontal, vertical);
        return false;
    }


    this.getFlash = function (message, expires_element, horizontal, vertical) {

        if (typeof(horizontal) == "undefined")
            horizontal = this.horizontal;

        if (typeof(vertical) == "undefined")
            vertical = this.vertical;

        if(expires_element === false)
            expires_element = "false";

        var tabMessage = message.split(this.getSeparateur());

        message = tabMessage[2];
        switch (tabMessage[1]) {

            case "valide":
            case "success":
                tabMessage[1] = "valide";
                entete = (tabMessage[3] == "false") ? '<div class="jquery-notific8-icon so100"><i class="icon-check"></i><span>Succès</span></div>' : tabMessage[3];
                break;
            case "erreur":
            case "error":
                tabMessage[1] = "erreur";
                entete = (tabMessage[3] == "false") ? '<div class="jquery-notific8-icon so100"><i class="icon-cross_mark"></i><span>Erreur</span></div>' : tabMessage[3];
                break;

            case "infos":
            case "info":
                tabMessage[1] = "infos";
                entete = (tabMessage[3] == "false") ? '<div class="jquery-notific8-icon so100"><i class="icon-information_white"></i><span>Informations</span></div>' : tabMessage[3];
                break;
            case "attention":
            case "warning":
                tabMessage[1] = "attention";
                entete = (tabMessage[3] == "false") ? '<div class="jquery-notific8-icon so100"><i class="icon-warning_alt"></i><span>Avertissement</span></div>' : tabMessage[3];
                break;

            default:
                tabMessage[1] = "valide";
                entete = (tabMessage[3] == "false") ? '<div class="jquery-notific8-icon so100"><i class="icon-check"></i><span>Succès</span></div>' : tabMessage[3];
                break;

        }

        $.notific8(message, {
            life: expires_element,
            heading: entete,
            theme: tabMessage[1],
            sticky: false,
            horizontalEdge: vertical,//inverse because the logic isn't logic
            verticalEdge: horizontal,
            zindex: 1500
        });

    }


}
