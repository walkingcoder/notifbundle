
##Installation de NotifBundle

# NotifBundle

Permet d'afficher les FlashBag messages sous forme de notification.

## Installation

Avoir installé préalablement Jquery et fontawesome.

A la racine du projet dans le fichier composer.json ajouter

```

 "require": {
 		"cpamg/notif-bundle": "~1.0"
         ....
    },

```

Puis le chemin vers le repositorie

```
"repositories": [
      {
        "type": "git",
        "url": "git@gitlabdev.cpam-bordeaux.cnamts.fr:JULIEN/notif-bundle.git"
      }
    ],
```

Ensuite lancer l'installation

```
composer install

```

Une fois l'installation réalisée, ajouter le bundle dans app/AppKernel.php

```

new Cpamg\NotifBundle\CpamgNotifBundle(),

```

Maintenant il faut installer les assets du bundle

```

php app/console assets:install

```

Dans votre layout ajouter le template twig associé à notif bundle
```

  {% include 'CpamgNotifBundle::init.html.twig' %}
```
### Fonctionnement sous symfony2
Dans symfony2 les messages de notification sont généralement réalisés avec les FlashBags.

http://symfony.com/doc/current/components/http_foundation/sessions.html

NotifBundle permet d'afficher un flashbag dans un thème flat design.

Dans votre controller appeler la classe notif

```
 $notif = $this->get("cpamg.notif");
 
```
Vous vous retrouvez avec un objet notif prêt à être setté :

| Options | Definitions |
|---------|-------------|
| setMessage | Le message qui sera affiché dans la notification |
| setType    | 4 types sont disponibles success, warning, info, error |
| setExpires | Durée d'affichage de la notification, temps en millisecondes, si false durée illimitée |
| setHeader  | Titre du message, par default un Titre est affiché par rapport au type de notification |
| setHorizontal  | 2 options left ou right |
| setVertical  | 2 options top ou bottom |

Exemple : Génération d'une notif type succes qui s'affichera 3000
```

$notif->setMessage("Mon message")->setType("success")->setExpires(3000);

```
Il suffit maintenant de transmettre l'objet dans votre flashBag symfony2

```

$this->get("session")->getFlashBag()->add("monFlash", $notif);

```
Vue d'ensemble

```
<?php
namespace CNAMTS\MONTEST\Bundle\Controller;
use CNAMTS\MONTEST\Bundle\Tableaux\TypeAssuranceTableau;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use SoBundle\SoNotification;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

class AppController extends MONTESTAbstractController
{
    
    /**
     *
     * @Route("/notif/store", name="store")
     */
    public function testNotif(Request $request)
    {

        $notif = $this->get("cpamg.notif");
        $notif->setMessage("Mon message")->setType("success")->setExpires(3000);
        $this->get("session")->getFlashBag()->add("monFlash", $notif);
                
        return $this->redirect($this->generateUrl("homepage"));
    }
}

```
Maintenant que votre FlashBag et créé vous pouvez la récupérer dans une autre méthode.

Exemple dans la méthode indexAction. Je récupére la flashBag si elle existe et l'envoi à ma vue.

```
<?php
namespace CNAMTS\MONTEST\Bundle\Controller;
use CNAMTS\MONTEST\Bundle\Tableaux\TypeAssuranceTableau;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use SoBundle\SoNotification;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

class AppController extends MONTESTAbstractController
{
    
    /**
     *
     * @Route("/notif/view", name="view")
     */
    public function indexAction(Request $request)
    {
        
        $flash = $this->get("session")->getFlashBag();
        $message = $flash->has("monFlash") ? $flash->get("monFlash") : "";
        
        return $this->render('AppBundle::show.html.twig', compact("message"));
    }
    
    /**
     *
     * @Route("/notif/store", name="store")
     */
    public function testNotif(Request $request)
    {

        $notif = $this->get("cpamg.notif");
        $notif->setMessage("Mon message")->setType("success")->setExpires(3000);
        $this->get("session")->getFlashBag()->add("monFlash", $notif);
                
        return $this->redirect($this->generateUrl("homepage"));
    }
}

```
Ma vue twig doit maintenant m'afficher ma FlashBag en Notification.
Pour cela il suffit d'afficher la variable avec le filtre notif()

```
   {{  message|notif() }}
   
```
notif() accepte 5 options (l'ordre doit être respecté)

| Options | Definitions |
|---------|-------------|
| type    | 4 types sont disponibles success, warning, info, error |
| expires | Durée d'affichage de la notification, temps en millisecondes, si false durée illimitée |
| horizontal  | 2 options left ou right |
| vertical  | 2 options top ou bottom |
| header  | Titre du message, par default un Titre est affiché par rapport au type de notification |

Vous pouvez definir depuis la vue twig l'affichage par défault de la notification
Exemple : Par default le message sera de type warning et s'affichera 5 secondes
```
   {{  message|notif('warning',5000) }}
   
```
Si dans votre controller, seul le message est setté, alors les options par default (depuis twig) seront appliquées lors du render.

```

$notif = $this->get("cpamg.notif");
$notif->setMessage("Mon message");
```
Un raccourci a été ajouté permettant d'envoyer à la flashBag non pas un objet notif mais un string

```

$this->get("session")->getFlashBag()->add("monFlash", "Mon message qui est un simple string");
```

### Fonctionnement en Javascript

NotifBundle s'appui sur le composant SoNotification pour fonctionner. Il est disponible en Javascript de cette manière

#### setFlash

| paramètres | Definitions |
|---------|-------------|
| message | Le message à afficher |
| type | 4 options disponibles : success, error, warning, info |
| header  | Titre du message, par default un Titre est affiché par rapport au type de notification |

#### getFlash
| paramètres | Definitions |
|---------|-------------|
| objet de setFlash | Objet retourné depuis setFlash |
| expires | Durée d'affichage de la notification, temps en millisecondes, si false durée illimitée |
| horizontal  | 2 options left ou right |
| vertical  | 2 options top ou bottom |


Exemple : Affiche une notification de type error s'affichant 5 secondes
```
var maFunctionJs = function(){
    
    var n = new SoNotification;
    n.getFlash(n.setFlash("Mon Message","error"),5000);

}


```
Exemple : Affiche une notification de type success s'affichant 8 secondes en bas à gauche avec titre "Mon Titre"
```
var maFunctionJs = function(){
    
    var n = new SoNotification;
    n.getFlash(n.setFlash("Mon Message","success","Mon Titre"),5000,"left","bottom");

}


```

### Fonctionnement en Ajax

Côté Php avec symfony2, il suffit de créer un objet notif et d'utiliser la fonction renderAjax()

Cette méthode renvoi le contenu en Json (par default)

```

$notif = $this->get("cpamg.notif");
$notif->setMessage("Mon message")->setType("error");

return new Response($notif->renderAjax());

//$notif->renderAjax(false) n'encode pas en Json

```

Côté Javascript, il suffit de definir que le type de retour est en Json et d'utiliser la méthode getFlashPhp

```

var testAjax = function () {
            $.ajax({
                type: "POST",
                url: Routing.generate('test_ajax'),
                data: "",
                dataType:"Json"
            }).done(function (data) {
            
            
                var n = new SoNotification();
                n.getFlashPhp(data);
                
                
            });
        }

```
Si vous souhaitez envoyer plusieurs données à votre vue il suffit de désactiver le Json

Exemple côté php :

```
$data["input"] = "une autre valeur";
$data["url"] = "/exemple";


$notif = $this->get("cpamg.notif");
$data["notification"] = $notif->setMessage("Mon message")->setType("error")->renderAjax(false);

return new Response(json_encode($data));
```

Côté Javascript

```

var testAjax = function () {
            $.ajax({
                type: "POST",
                url: Routing.generate('test_ajax'),
                data: "",
                dataType:"Json"
            }).done(function (data) {
            
                $('#moninput').val(data.input);
                
                var n = new SoNotification();
                n.getFlashPhp(data.notification);
                                
            });
        }

```















>>>>>>> 59dc2be961f3725a7bdc085a52748e4a8d2883ec
